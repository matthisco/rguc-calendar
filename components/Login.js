import React, { Component } from "react";
import PropTypes from "prop-types";

import {
    StyleSheet,
    Text,
    View,
    TextInput,
    ImageBackground,
    TouchableHighlight,
    Platform
} from "react-native";

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            password: ""
        };
    }

    render() {
        const { onPress } = this.props;

        return (
            <ImageBackground
                source={require("../img/bkgPhoto.jpg")}
                style={{
                    flex: 1,
                    width: null,
                    height: null
                }}
            >
                <TextInput
                    style={styles.input}
                    underlineColorAndroid="rgba(0,0,0,0)"
                    onChangeText={text => this.setState({ password: text })}
                    placeholder="Please enter password"
                />

                <TouchableHighlight
                    style={{
                        alignItems: "center",
                        backgroundColor: "#006666",
                        padding: 10,
                        width: Platform.OS === "ios" ? "100%" : "90%",
                        alignSelf: "center"
                    }}
                    onPress={() => onPress(this.state.password)}
                >
                    <Text style={{ color: "white" }}>Submit</Text>
                </TouchableHighlight>
            </ImageBackground>
        );
    }
}
const styles = StyleSheet.create({
    input: {
        margin: 15,
        height: 40,
        borderColor: "#0066cc",
        backgroundColor: "white"
    }
});

export default Login;
