import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  Text,
  View,
  StyleSheet,
  Image,
  Alert,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import * as helpers from '../../helpers';
import {fetchEvents, eventsForAgendaMonth} from '../../actions/events';

import XDate from 'xdate';
import AgendaItem from './AgendaItem';
import CustomHeader from '../../navigation/CustomHeader';
import EventCalendar from 'react-native-events-calendar';
import propTypes from 'prop-types';

class AgendaScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedDay: '',
    };
    this.dateChanged = this.dateChanged.bind(this);
    this.renderEvent = this.renderEvent.bind(this);
  }
  componentDidMount() {
    const {
      fetchEvents,
      eventsMonth,
      route: {
        params: {
          day: {timestamp: day},
        },
      },
    } = this.props;
  }

  dateChanged(date) {
    this.setState({selectedDay: date});
  }

  renderEvent(event) {
    return <AgendaItem {...event} />;
  }

  render() {
    const {
      fetchEvents,
      route: {
        params: {
          day: {dateString: selected},
        },
      },
    } = this.props;
    const currentDate = helpers.currentDate;
    let {width} = Dimensions.get('window');

    let events =
      fetchEvents[this.state.selectedDay || selected] &&
      fetchEvents[this.state.selectedDay || selected].dots;

    if (!events) {
      events = [];
    }

    return (
      <>
        <CustomHeader {...this.props} />
        <EventCalendar
          events={events}
          width={width}
          start={7}
          end={18}
          eventTapped={this.eventTapped}
          headerIconLeft={<Icon name="angle-left" size={25} color="#666" />}
          headerIconRight={<Icon name="angle-right" size={25} color="#666" />}
          renderEvent={this.renderEvent}
          dateChanged={this.dateChanged}
          initDate={selected}
          scrollToFirst
          upperCaseHeader
          uppercase
          styles={styles}
          scrollToFirst={false}
        />
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    fetchEvents: state.fetchEvents,
  };
};

export default connect(mapStateToProps)(AgendaScreen);

AgendaScreen.propTypes = {
  eventsMonth: propTypes.object,
  eventsForYear: propTypes.object,
};

const styles = {
  container: {
    backgroundColor: '#00000000',
  },
  event: {
    backgroundColor: '#00000000',
    borderColor: '#DDE5FD',
    borderWidth: 0,
    borderRadius: 0,
    paddingTop: 3,
    paddingBottom: 2,
  },
  header: {
    height: 30,
    paddingHorizontal: 30,
  },
  headerText: {},
};
