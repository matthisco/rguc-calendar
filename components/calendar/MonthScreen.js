import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as helpers from '../../helpers';
import {
  Text,
  StyleSheet,
  ScrollView,
  View,
  Alert,
  AsyncStorage,
  ActivityIndicator,
} from 'react-native';
import {CalendarList} from 'react-native-calendars';
import CustomHeader from '../../navigation/CustomHeader';

import {hasErrored} from '../../actions/loader';

import {
  fetchEvents,
  eventsForCalendarMonth,
  resetForm,
} from '../../actions/events';

import {
  NavigationContainer,
  DefaultTheme,
  CommonActions,
} from '@react-navigation/native';

class MonthScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.onDayPress = this.onDayPress.bind(this);
    this.loadItems = this.loadItems.bind(this);
    this.alertConfirm = this.alertConfirm.bind(this);
  }

  onDayPress(day) {
    const {
      navigation: {navigate},
    } = this.props;
    navigate('Day', {day});
  }

  loadItems(months) {
    const {events, eventsForMonth} = this.props;

    eventsForMonth(events, months);
  }

  alertConfirm() {
    const {
      navigation: {navigate},
      resetForm,
    } = this.props;
    hasErrored(false);
    navigate('Home');
  }

  render() {
    const currentDate = helpers.currentDate;

    const {
      events,
      isLoading,
      hasErrored,
      navigation,
      eventsMonth,
      dispatch,
      resetForm,
      eventsForMonth,
    } = this.props;

    if (hasErrored && Object.keys(events).length == 0) {
      return (
        <View style={styles.container}>
          <Text>
            This calendar is not in use, please logout and choose another
            calendar.
          </Text>
        </View>
      );
    }

    if (isLoading || Object.keys(events).length == 0) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#6da1a6" />
        </View>
      );
    }

    return (
      <>
        <CustomHeader {...this.props} />
        <CalendarList
          current={currentDate}
          markingType={'multi-dot'}
          onDayPress={this.onDayPress}
          minDate={currentDate}
          loadingIndicatorColor={'#6da1a6'}
          markedDates={events}
          pastScrollRange={0}
          futureScrollRange={24}
          scrollEnabled={true}
          showScrollIndicator={true}
        />
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    events: state.fetchEvents,
    eventsMonth: state.eventsForMonth,
    hasErrored: state.hasErrored,
    isLoading: state.isLoading,
    credentials: state.setCredentials,
  };
};

const mapDispatchToProps = dispatch => ({
  resetForm: () => dispatch(resetForm()),
  fetchEvents: id => dispatch(fetchEvents(id)),
  eventsForMonth: (events, months) =>
    dispatch(eventsForCalendarMonth(events, months)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MonthScreen);

const styles = StyleSheet.create({
  calendar: {
    paddingTop: 5,
    flex: 1,
  },

  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
