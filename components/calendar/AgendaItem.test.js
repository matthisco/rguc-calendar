import React from "react";
import { mount, shallow } from "enzyme";
import { expect } from "chai";
import AgendaItem from "./AgendaItem";
jest.mock("Dimensions");
jest.mock("moment", () => () => ({ format: () => "2018–01–30T12:34:56" }));

// jest.mock("isIphoneX");
describe("<AgendaItem />", () => {
	it("it should render 2 text components", () => {
		const props = {
			summary: "test title",
			start: "8-00",
			end: "9-00"
		};
		
		const wrapper = shallow(<AgendaItem {...props} />);

		expect(wrapper.find("Text"));
	});
});
