import React from "react";
import { shallow, mount, render } from "enzyme";
import HomeScreen from "./HomeScreen";
import renderer from "react-test-renderer";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";

const mockStore = configureMockStore();

describe("Homescreen Component", () => {
    it("should render without throwing an error", () => {
        const store = mockStore({
            credentials: {
                year: 1,
                group: 1,
                student: 2,
                showStudent: true
            }
        });
        const navigation = { navigate: jest.fn() };

        const tree = renderer
            .create(
                <Provider store={store} navigation={navigation}>
                    <HomeScreen />
                </Provider>
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});
