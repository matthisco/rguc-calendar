export const currentDate = new Date().toISOString().split("T")[0];

export const currentDateString = new Date().toISOString();

export function formatDate(start) {
	const date = new Date(start);

	return date.toISOString().split("T")[0];
}

export function timeToString(timestamp, index = 0) {
	if (typeof timestamp === "string") return timestamp;

	const time = timestamp + index * 24 * 60 * 60 * 1000;
	const date = new Date(time);

	return date.toISOString().split("T")[0];
}

export function checkDate(timestamp) {
	const date = new Date(timestamp);
	return isNaN(date);
}
