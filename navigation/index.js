import React, { Component } from "react";

import {
  BackHandler,
  View,
  StatusBar,
  Platform,
  StyleSheet,
  Dimensions
} from "react-native";

import { ifIphoneX } from 'react-native-iphone-x-helper'

const { height, width } = Dimensions.get("window");


import NavigationStack from "./navigationStack";

class AppNavigation extends Component {

  render() {
    const { navigationState, dispatch } = this.props;
    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" backgroundColor="#006666" />
        <View style={styles.appBar} />
        <View style={styles.content}>
          <NavigationStack />
        </View>
      </View>
    );
  }
}



export default AppNavigation;

const STATUSBAR_HEIGHT =
  Platform.OS === "ios" ? 20 : StatusBar.currentHeight + 1;
const APPBAR_HEIGHT = Platform.OS === "ios" ? 23 : 0;


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  statusBar: {
    height: STATUSBAR_HEIGHT
  },
  appBar: {
    backgroundColor: "#006666",
    height: APPBAR_HEIGHT,
    ...ifIphoneX({
      height: 35
    })

  },
  content: {
    flex: 1,
    backgroundColor: "#33373B"
  }
});
