import React, {Component} from 'react';
import {
	Image,
	View,
	Text,
	Modal,
	Button,
	TouchableOpacity,
	AsyncStorage,
	StyleSheet,
	Platform,
	Alert,
	Dimensions,
	TouchableHighlight,
} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer, DefaultTheme} from '@react-navigation/native';
import CustomHeader from './CustomHeader';
import HomeScreen from '../components/HomeScreen';
import MonthScreen from '../components/calendar/MonthScreen';
import AgendaScreen from '../components/calendar/AgendaScreen';

const appTheme = {
	...DefaultTheme,
	colors: {
		...DefaultTheme.colors,
		// primary: '#00003390',
		card: '#00003399',
		background: '#00003350',
	},
};



const Stack = createStackNavigator();

export default function AppNavigator() {
	return (
		<NavigationContainer >
			<Stack.Navigator
				initialRouteName="Home"
				screenOptions={{
					headerShown: false,
				}}>
				<Stack.Screen name="Home" component={HomeScreen} />
				<Stack.Screen name="Month" component={MonthScreen} />
				<Stack.Screen name="Day" component={AgendaScreen} />
			</Stack.Navigator>
		</NavigationContainer>
	);
}
