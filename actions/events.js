import {AsyncStorage} from 'react-native';
import moment from 'moment';
import {
  isLoading,
  hasErrored,
  fetchSuccessCategories,
  fetchSuccessEvents,
  isLoadingCredentials,
  fetchErrorEvents,
} from './loader';

import {createStackNavigator} from '@react-navigation/stack';
import {
  NavigationContainer,
  DefaultTheme,
  CommonActions,
} from '@react-navigation/native';

import * as helpers from '../helpers';
import {data, key} from '../config/calendars.js';
export function fetchEvents(id) {
  const currentDateString =
    moment().format('YYYY-MM-DD') + 'T' + '07:00:00.000Z';

  const url = `https://www.googleapis.com/calendar/v3/calendars/${id}/events?singleEvents=true&orderBy=startTime&timeMin=${currentDateString}&key=${key}`;

  console.log(url);

  return dispatch => {
    dispatch(isLoading(true));
    fetch(url)
      .then(response => {
        return response;
      })
      .then(response => response.json())
      .then(data => {
        const {error} = data;
        if (error) {
          dispatch(hasErrored(error.message));
        } else {
          dispatch(fetchSuccessEvents(data.items));
        }

        CommonActions.navigate('Month');
        dispatch(isLoading(false));
      });
  };
}

export const setCredentials = data => ({
  type: 'SET_CREDENTIALS',
  data,
});

export function getCredentials() {
  return dispatch => {
    AsyncStorage.getItem('loggedIn')
      .then(this.props.dispatch(isLoadingCredentials(true)))
      .then(data =>
        data
          ? this.props
              .dispatch(setCredentials(JSON.parse(data)))
              .then(this.props.dispatch(navigate('Month')))
              .then(this.props.dispatch(isLoadingCredentials(false)))
          : this.props.dispatch(isLoadingCredentials(false)),
      );
  };
}

export const filterEvents = (data, id) => ({
  type: 'FILTER_EVENTS',
  data,
  id,
});

export const eventsForAgendaMonth = (data, day) => ({
  type: 'EVENTS_AGENDA_MONTH',
  data,
  day,
});

export const eventsForCalendarMonth = (data, day) => ({
  type: 'EVENTS_CALENDAR_MONTH',
  data,
  day,
});

export const setYear = int => ({
  type: 'SET_YEAR',
  year: int,
});

export const setGroup = (group, data, year, label) => ({
  type: 'SET_GROUP',
  group,
  data,
  year,
  label,
});

export const setStudent = (id, label) => ({
  type: 'SET_STUDENT',
  id,
  label,
});

export const setAuthenticated = int => ({
  type: 'SET_AUTH',
  authentication: int,
});

export const clearCredentials = () => ({
  type: 'CLEAR_CREDENTIALS',
});

export const resetData = () => ({
  type: 'RESET_DATA',
});

export function resetForm() {
  return dispatch => {
    dispatch(clearCredentials());
  };
}

export const changeMonth = month => ({
  type: 'CHANGE_MONTH_NAME',
  month,
});

export function fetchNames() {
  const url = `https://www.googleapis.com/calendar/v3/calendars/fourthyear-spc@rguc.co.uk/events?nextPageToken=nextPageToken=2500&key=AIzaSyB77rzW-VzrZAaWJzgiG1Gf-MGEcwn-a-E`;
  return dispatch => {
    dispatch(isLoading(true));
    fetch(url)
      .then(response => {
        return response;
      })
      .then(response => response.json())
      .then(data => {
        console.log(data);
      });
  };
}
