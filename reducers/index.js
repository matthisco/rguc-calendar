import { combineReducers } from "redux";
import {
	setCredentials,
	fetchNames,
	eventsForMonth,
	fetchEvents,
	changeMonth,
	authenticated
} from "./events";

import { hasErrored, isLoading, isLoadingCredentials } from "./loader";

const rootReducer = combineReducers({
	fetchEvents,
	hasErrored,
	eventsForMonth,
	changeMonth,
	isLoading,
	isLoadingCredentials,
	fetchNames,
	setCredentials,
	authenticated
});

export default rootReducer;
